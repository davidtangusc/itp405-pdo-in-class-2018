<?php
  $pdo = new PDO('sqlite:chinook.db');
  $sql = '
    select Quantity, invoice_items.UnitPrice as UnitPrice, artists.Name as artistName, tracks.Name as trackName
    from invoice_items
    inner join tracks
    on invoice_items.TrackId = tracks.TrackId
    inner join albums
    on albums.AlbumId = tracks.AlbumId
    inner join artists
    on artists.ArtistId = albums.ArtistId
    where InvoiceId = ?
  ';
  $statement = $pdo->prepare($sql);
  $statement->bindParam(1, $_GET['invoice']);
  $statement->execute();
  $invoiceItems = $statement->fetchAll(PDO::FETCH_OBJ);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Invoice Details</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css">
  </head>
  <body>
    <table class="table">
      <tr>
        <th>Song</th>
        <th>Quantity</th>
        <th>Unit Price</th>
      </tr>
      <?php foreach($invoiceItems as $invoiceItem) : ?>
        <tr>
          <td>
            <?php echo $invoiceItem->trackName ?>
            <span>by</span>
            <?php echo $invoiceItem->artistName ?>
          </td>
          <td><?php echo $invoiceItem->Quantity ?></td>
          <td><?php echo $invoiceItem->UnitPrice ?></td>
        </tr>
      <?php endforeach ?>
    </table>
  </body>
</html>
