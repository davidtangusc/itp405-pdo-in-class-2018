<?php
  $pdo = new PDO('sqlite:chinook.db');
  $sql = '
    select *
    from invoices
    inner join customers
    on invoices.CustomerId = customers.CustomerId
    order by InvoiceDate desc
    limit 10
  ';
  // $sql = '
  //   select *
  //   from invoices, customers
  //   where invoices.CustomerId = customers.CustomerId
  //   limit 10
  // ';
  $statement = $pdo->prepare($sql);
  $statement->execute();
  $invoices = $statement->fetchAll(PDO::FETCH_OBJ);
  // var_dump($invoices);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Invoices</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css">
  </head>
  <body>
    <table class="table">
      <tr>
        <th>Date</th>
        <th>Last Name</th>
        <th colspan="2">First Name</th>
      </tr>
      <?php foreach($invoices as $invoice) : ?>
        <tr>
          <td><?php echo $invoice->InvoiceDate ?></td>
          <td><?php echo $invoice->LastName ?></td>
          <td><?php echo $invoice->FirstName ?></td>
          <td>
            <a href="invoice-details.php?invoice=<?php echo $invoice->InvoiceId ?>">
              Details
            </a>
          </td>
        </tr>
      <?php endforeach ?>
    </table>
  </body>
</html>
